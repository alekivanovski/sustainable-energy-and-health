# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 17:54:02 2020

@author: Nikola
"""
import pandas as pd
#%%
#Load data
import pickle
with open('data_v2_integrated/public_health_data.pkl', 'rb') as f:
    ph_indicators = pickle.load(f)

with open('data_v2_integrated/renewable_energy_data.pkl', 'rb') as f:
    re_indicators = pickle.load(f)
    
del f
#%%
ph_dict = {}
re_dict = {}
#Convert missing values to a non-castable value
for indicator in ph_indicators:
    ph_dict[indicator['Indicator'].unique()[0]]=indicator.fillna('..')
    
for indicator in re_indicators:
    re_dict[indicator['Indicator'].unique()[0]]=indicator.fillna('..')
#%%
#Remove years not in range [2005-2015]
indicator_columns = [  'Country', 'Indicator',       2005,        2006,        2007,
              2008,        2009,        2010,        2011,        2012,
              2013,        2014,        2015]
for indicator_id in ph_dict.keys():
    indicator = ph_dict[indicator_id]
    if '2006 [YR2006]' in indicator.columns:
        indicator.rename(columns = {'2006 [YR2006]':2006}, inplace = True) 
    #print(indicator.columns)
    ph_dict[indicator_id] = indicator[indicator_columns]

for indicator_id in re_dict.keys():
    indicator = re_dict[indicator_id]
    if '2006 [YR2006]' in indicator.columns:
        indicator.rename(columns = {'2006 [YR2006]':2006}, inplace = True) 
    #print(indicator.columns)
    re_dict[indicator_id] = indicator[indicator_columns]
#%%
#Check if string is float
def isfloat(value):
    try:
      float(value)
      return True
    except ValueError:
      return False

def get_missing_values_matrix(indicator_group):
    missing_matrix = {}
    for indicator in indicator_group.keys():
        #Create copy of data
        ind_df = indicator_group[indicator].copy()
        #Get only the years from the columns
        years = [column for column in ind_df.columns if not isinstance(column, str)]
        #Iterate over the years
        for year in years:
            #Get bool if value is missing
            ind_df[year] = ind_df[year].apply(lambda x:isfloat(x))
        missing_matrix[indicator] = ind_df
    return missing_matrix
    
ph_missing = get_missing_values_matrix(ph_dict)
re_missing = get_missing_values_matrix(re_dict)
#%%
def count_missing_by_country_and_indicator(indicator_group):
    missing_counts = {}
    for indicator in indicator_group.keys():
        ind_df = indicator_group[indicator].copy()
        ##Get by year
        #Get only the years from the columns
        years = [column for column in ind_df.columns if not isinstance(column, str)]
        missing_by_year = ind_df[years].value_counts()
        ##Get by country
        missing_by_country = pd.concat([ind_df['Country'],ind_df[years].sum(axis=1)],axis=1)
        missing_counts[indicator] = (missing_by_year,missing_by_country)
    return missing_counts

ph_missing_counts = count_missing_by_country_and_indicator(ph_missing)
re_missing_counts = count_missing_by_country_and_indicator(re_missing)
#%%
# =============================================================================
# #Show missing value counts for Renewable Energy Indicators
# for indicator in list(re_missing_counts.keys()):
#     missing_by_year = re_missing_counts[indicator][0]
#     missing_by_country = re_missing_counts[indicator][1]
#     print(indicator)
#     print(missing_by_year)
#     print(missing_by_country)
# #%%
# #Show missing value counts for Public Health Indicators
# for indicator in list(ph_missing_counts.keys()):
#     missing_by_year = ph_missing_counts[indicator][0]
#     missing_by_country = ph_missing_counts[indicator][1]
#     print(indicator)
#     print(missing_by_year)
#     print(missing_by_country)
# =============================================================================

#%%
def missing_values_percentage(indicator_group):
    missing_dict = {}
    for indicator in indicator_group.keys():
        ind_df = indicator_group[indicator]
        ind_year = ind_df[0]
        ind_count_df = pd.DataFrame(columns = ['Num Countries',
                                       'Num Values',
                                       'Values %'])
        for index, value in ind_year.items():
            ind_count_df = ind_count_df.append({'Num Countries':value,
                            'Num Values':sum(index),
                            'Values %':sum(index)/len(index)}, ignore_index=True)
        missing_dict[indicator] = ind_count_df
    return missing_dict

ph_percentage = missing_values_percentage(ph_missing_counts)
re_percentage = missing_values_percentage(re_missing_counts)

# =============================================================================
# #Print percentage of missing values by indicator
# for indicator_id in ph_percentage.keys():
#     indicator = ph_percentage[indicator_id]
#     print(indicator_id)
#     print(indicator)
#     
# for indicator_id in re_percentage.keys():
#     indicator = re_percentage[indicator_id]
#     print(indicator_id)
#     print(indicator)    
# =============================================================================
    
#%%
#Remove indicators with missing values for
#largest number of countries
ph_ind_del = []
re_ind_del = []
for indicator_id in ph_percentage.keys():
    indicator = ph_percentage[indicator_id]
    #Get the number of non-missing values
    #for the largest number of countries (first row)
    #print(indicator['Values %'].iloc[0])
    if indicator['Values %'].iloc[0] < 0.9:
        ph_ind_del.append(indicator_id)

for indicator_id in re_percentage.keys():
    indicator = re_percentage[indicator_id]
    #Get the number of non-missing values
    #for the largest number of countries (first row)
    #print(indicator['Values %'].iloc[0])
    if indicator['Values %'].iloc[0] < 0.9:
        re_ind_del.append(indicator_id)

print("Removed indicators:")
#Remove those indicators:
for indicator_id in ph_ind_del:
    print(indicator_id)
    ph_dict.pop(indicator_id)
    ph_missing.pop(indicator_id)
    ph_missing_counts.pop(indicator_id)
    ph_percentage.pop(indicator_id)
    
    
for indicator_id in re_ind_del:
    print(indicator_id)
    re_dict.pop(indicator_id)
    re_missing.pop(indicator_id)
    re_missing_counts.pop(indicator_id)
    re_percentage.pop(indicator_id)
#%%
#Remove countries with more than 1 missing value
def country_non_missing_values_filter(missing_matrix_group):
    country_filters = {}
    for indicator_id in missing_matrix_group.keys():
        indicator = missing_matrix_group[indicator_id]
        #Get only the years from the columns
        years = [column for column in indicator.columns if not isinstance(column, str)]
        #Get only the countries with <=1 missing value
        new_indicator = indicator[indicator[years].sum(axis=1)==10]
        new_indicator_1 = indicator[indicator[years].sum(axis=1)==11]
        country_1_missing_filter = new_indicator['Country']
        country_0_missing_filter = new_indicator_1['Country']
        country_filters[indicator_id] = (country_1_missing_filter,country_0_missing_filter)
    return country_filters

ph_country_non_missing = country_non_missing_values_filter(ph_missing)
re_country_non_missing = country_non_missing_values_filter(re_missing)

#Apply mask
for indicator_id in ph_country_non_missing.keys():
    indicator = ph_dict[indicator_id]
    country_1_missing_mask = ph_country_non_missing[indicator_id][0]
    country_0_missing_mask = ph_country_non_missing[indicator_id][1]
    #print(indicator_id)
    #print(indicator.shape)
    indicator = indicator[indicator['Country'].isin(country_1_missing_mask) | indicator['Country'].isin(country_0_missing_mask)]
    #print(indicator.shape)
    ph_dict[indicator_id] = indicator

for indicator_id in re_country_non_missing.keys():
    indicator = re_dict[indicator_id]
    country_1_missing_mask = re_country_non_missing[indicator_id][0]
    country_0_missing_mask = re_country_non_missing[indicator_id][1]
    #print(indicator_id)
    #print(indicator.shape)
    indicator = indicator[indicator['Country'].isin(country_1_missing_mask) | indicator['Country'].isin(country_0_missing_mask)]
    #print(indicator.shape)
    re_dict[indicator_id] = indicator
#%%
#Find the single missing values and fill them or remove them
ind_country_1_missing = {}
for indicator_id in ph_country_non_missing.keys():
    country_1_missing_mask = ph_country_non_missing[indicator_id][0]
# =============================================================================
#     country_0_missing_mask = ph_country_non_missing[indicator_id][1]
#     print(indicator_id)
#     print(country_1_missing_mask.shape)
#     print(country_0_missing_mask.shape)
# =============================================================================
    if not country_1_missing_mask.empty:
        indicator = ph_dict[indicator_id]
        ind_1_missing = indicator[indicator['Country'].isin(country_1_missing_mask)]
        ind_country_1_missing[indicator_id] = ind_1_missing
        
    
for indicator_id in re_country_non_missing.keys():
    country_1_missing_mask = re_country_non_missing[indicator_id][0]
# =============================================================================
#     country_0_missing_mask = re_country_non_missing[indicator_id][1]
#     print(indicator_id)
#     print(country_1_missing_mask.shape)
#     print(country_0_missing_mask.shape)
# =============================================================================
    if not country_1_missing_mask.empty:
        indicator = re_dict[indicator_id]
        ind_1_missing = indicator[indicator['Country'].isin(country_1_missing_mask)]
        ind_country_1_missing[indicator_id] = ind_1_missing
#%%
#Convert missing values to NaN
def nonfloat_to_NaN(value):
    try:
      float(value)
      return value
    except ValueError:
      return None
def fix_percentage_outliers(data,indicator_id):
    percentage_based_indicators = ['Access to clean fuels and technologies for cooking (% of population)',
 'Access to electricity (% of population)',
 'Access to electricity, rural (% of rural population)',
 'Access to electricity, urban (% of urban population)',
 'Alternative and nuclear energy (% of total energy use)',
 'CO2 emissions from electricity and heat production, total (% of total fuel combustion)',
 'CO2 emissions from gaseous fuel consumption (% of total)',
 'CO2 emissions from liquid fuel consumption (% of total)',
 'CO2 emissions from manufacturing industries and construction (% of total fuel combustion)',
 'CO2 emissions from other sectors, excluding residential buildings and commercial and public services (% of total fuel combustion)',
 'CO2 emissions from residential buildings and commercial and public services (% of total fuel combustion)',
 'CO2 emissions from solid fuel consumption (% of total)',
 'CO2 emissions from transport (% of total fuel combustion)',
 'Combustible renewables and waste (% of total energy)',
 'Electric power transmission and distribution losses (% of output)',
 'Electricity production from coal sources (% of total)',
 'Electricity production from hydroelectric sources (% of total)',
 'Electricity production from natural gas sources (% of total)',
 'Electricity production from nuclear sources (% of total)',
 'Electricity production from oil sources (% of total)',
 'Electricity production from oil, gas and coal sources (% of total)',
 'Electricity production from renewable sources, excluding hydroelectric (% of total)',
 'Fossil fuel energy consumption (% of total)',
 'Renewable electricity output (% of total electricity output)',
 'Renewable energy consumption (% of total final energy consumption)',
 'Renewable energy share of TFEC (%)',
 ]
    non_negative_indicators = ['CO2 emissions (kg per 2010 US$ of GDP)',
 'CO2 emissions (kg per 2017 PPP $ of GDP)',
 'CO2 emissions (kg per PPP $ of GDP)',
 'CO2 emissions (kt)',
 'CO2 emissions (metric tons per capita)',
 'CO2 emissions from gaseous fuel consumption (kt)',
 'CO2 emissions from liquid fuel consumption (kt)',
 'CO2 emissions from solid fuel consumption (kt)',
 'CO2 intensity (kg per kg of oil equivalent energy use)',
 'Electric power consumption (kWh per capita)',
 'Electricity production from renewable sources, excluding hydroelectric (kWh)',
 'Energy intensity level of primary energy (MJ/$2011 PPP GDP)',
 'Energy use (kg of oil equivalent per capita)',
 'Energy use (kg of oil equivalent) per $1,000 GDP (constant 2017 PPP)',
 'GDP per unit of energy use (constant 2017 PPP $ per kg of oil equivalent)',
 'GDP per unit of energy use (PPP $ per kg of oil equivalent)',
 'Renewable energy consumption (TJ)',
 'Total electricity output (GWh)',
 'Total final energy consumption (TFEC) (TJ)',
 'Renewable electricity output (GWh)']
    if indicator_id in percentage_based_indicators:
        data[data<0] = 0
        data[data>100] = 100
    if indicator_id in non_negative_indicators:
        data[data<0] = 0
    return data
    
    
def interpolate_missing_values(data,indicator_id):
    ind_years = data.transpose()
    ind_years.index = ind_years.index.astype(int)
    ind_years = ind_years.interpolate(method = "spline",
                                          order = 1,
                                          limit = 10, 
                                          limit_direction = 'both')
    ind_years = ind_years.transpose()
    ind_years = fix_percentage_outliers(ind_years,indicator_id)
    return ind_years


interpolated_missing = {}
for indicator_id in ind_country_1_missing.keys():
    #print(indicator_id)
    ind_df = ind_country_1_missing[indicator_id].copy()
    years = [column for column in ind_df.columns if not isinstance(column, str)]
    #Iterate over the years
    for year in years:
        #Convert missing value to NaN
        ind_df[year] = ind_df[year].apply(lambda x:nonfloat_to_NaN(x))
        ind_df[year] = ind_df[year].astype(float)
    #Interpolate missing values
    ind_years_interp = interpolate_missing_values(ind_df[years],indicator_id)
    ind_df[years] = ind_years_interp
    interpolated_missing[indicator_id] = ind_df

#%%
#Add interpolated missing values to original data
def insert_interpolated_data(original_data,interpolated_data):
    complete_data = {}
    for indicator_id in original_data.keys():
        indicator = original_data[indicator_id].copy()
        if indicator_id in interpolated_data:
            #Iterate over all countries with interpolated data
            indicator_interp = interpolated_data[indicator_id]
            for country in indicator_interp['Country'].unique():
                #Copy the interpolated data
                indicator[indicator['Country'] == country] = indicator_interp[indicator_interp['Country'] == country]
        complete_data[indicator_id] = indicator
    return complete_data
    
ph_complete = insert_interpolated_data(ph_dict,interpolated_missing)
re_complete = insert_interpolated_data(re_dict,interpolated_missing)
#%%
# =============================================================================
# #Test check, if there are no missing values
# ph_complete_missing = get_missing_values_matrix(ph_complete)
# re_complete_missing = get_missing_values_matrix(re_complete)
# 
# ph_complete_missing_counts = count_missing_by_country_and_indicator(ph_complete_missing)
# re_complete_missing_counts = count_missing_by_country_and_indicator(re_complete_missing)
# 
# for indicator_id in re_complete_missing_counts.keys():
#     indicator_mat = re_complete_missing_counts[indicator_id]
#     print(indicator_id)
#     print(indicator_mat[0])
# =============================================================================
#%%
#TODO
#Create country filter
country_list_by_indicator = {}
for indicator_id in ph_complete.keys():
    indicator = ph_complete[indicator_id]
    country_list_by_indicator[indicator_id] = set(indicator['Country'])
for indicator_id in re_complete.keys():
    indicator = re_complete[indicator_id]
    country_list_by_indicator[indicator_id] = set(indicator['Country'])

#%%
#Assign any indicator country set
#Get countries that are present in all indicators
country_intersection = country_list_by_indicator[indicator_id]
for indicator_id in country_list_by_indicator.keys():
    country_set = country_list_by_indicator[indicator_id]
    country_intersection = country_intersection & country_set

#%%
#Get countries that are present in some indicators
country_outersection = set()
for indicator_id in country_list_by_indicator.keys():
    country_set = country_list_by_indicator[indicator_id]
    country_outersection = country_outersection | (country_set - country_intersection)

#%%
import copy
#Merge different country names for same country
country_list_by_indicator_mod = {}
for indicator_id in country_list_by_indicator.keys():
    country_set = copy.deepcopy(country_list_by_indicator[indicator_id])
    if 'Yemen, Rep.' in country_set:
        country_set.remove('Yemen, Rep.')
        country_set.update({'Yemen'})
    if 'Viet Nam' in country_set:
        country_set.remove('Viet Nam')
        country_set.update({'Vietnam'})
    if 'Venezuela (Bolivarian Republic of)' in country_set:
        country_set.remove('Venezuela (Bolivarian Republic of)')
        country_set.update({'Venezuela, RB'})
    if 'United States' in country_set:
        country_set.remove('United States')
        country_set.update({'United States of America'})
    if 'United Kingdom of Great Britain and Northern Ireland' in country_set:
        country_set.remove('United Kingdom of Great Britain and Northern Ireland')
        country_set.update({'United Kingdom'})
    if 'Slovak Republic' in country_set:
        country_set.remove('Slovak Republic')
        country_set.update({'Slovakia'})
    if 'Republic of North Macedonia' in country_set:
        country_set.remove('Republic of North Macedonia')
        country_set.update({'North Macedonia'})
    if 'Republic of Moldova' in country_set:
        country_set.remove('Republic of Moldova')
        country_set.update({'Moldova'})
    if 'Republic of Korea' in country_set:
        country_set.remove('Republic of Korea')
        country_set.update({'Korea, Rep.'})
    if 'Macedonia, FYR' in country_set:
        country_set.remove('Macedonia, FYR')
        country_set.update({'North Macedonia'})
    if 'Kyrgyz Republic' in country_set:
        country_set.remove('Kyrgyz Republic')
        country_set.update({'Kyrgyzstan'})
    if 'Gambia, The' in country_set:
        country_set.remove('Gambia, The')
        country_set.update({'Gambia'})
    if 'Egypt, Arab Rep.' in country_set:
        country_set.remove('Egypt, Arab Rep.')
        country_set.update({'Egypt'})
    if 'Bolivia (Plurinational State of)' in country_set:
        country_set.remove('Bolivia (Plurinational State of)')
        country_set.update({'Bolivia'})
    if 'Bahamas, The' in country_set:
        country_set.remove('Bahamas, The')
        country_set.update({'Bahamas'})
    country_list_by_indicator_mod[indicator_id] = country_set
#%%
#Assign any indicator country set
country_intersection_mod = country_list_by_indicator_mod[indicator_id]
for indicator_id in country_list_by_indicator_mod.keys():
    country_set = country_list_by_indicator_mod[indicator_id]
    country_intersection_mod = country_intersection_mod & country_set
#%%
#Get renamed countries which were present in all indicators
renamed_countries = country_intersection_mod - country_intersection
#%%
ph_final = {}
re_final = {}
for indicator_id in ph_complete.keys():
    indicator = ph_complete[indicator_id]
    indicator = indicator.replace({'Bolivia (Plurinational State of)':'Bolivia',
                             'Egypt, Arab Rep.':'Egypt',
                             'Republic of Korea':'Korea, Rep.',
                             'Kyrgyz Republic':'Kyrgyzstan',
                             'Republic of Moldova':'Moldova',
                             'Macedonia, FYR':'North Macedonia',
                             'Republic of North Macedonia':'North Macedonia',
                             'Slovak Republic':'Slovakia',
                             'United Kingdom of Great Britain and Northern Ireland':'United Kingdom',
                             'United States':'United States of America'})
    indicator = indicator[indicator['Country'].isin(country_intersection_mod)]
    ph_final[indicator_id] = indicator
for indicator_id in re_complete.keys():
    indicator = re_complete[indicator_id]
    indicator = indicator.replace({'Bolivia (Plurinational State of)':'Bolivia',
                             'Egypt, Arab Rep.':'Egypt',
                             'Republic of Korea':'Korea, Rep.',
                             'Kyrgyz Republic':'Kyrgyzstan',
                             'Republic of Moldova':'Moldova',
                             'Macedonia, FYR':'North Macedonia',
                             'Republic of North Macedonia':'North Macedonia',
                             'Slovak Republic':'Slovakia',
                             'United Kingdom of Great Britain and Northern Ireland':'United Kingdom',
                             'United States':'United States of America'})
    indicator = indicator[indicator['Country'].isin(country_intersection_mod)]
    re_final[indicator_id] = indicator
#%%
# =============================================================================
# #Test to confirm
# for indicator_id in ph_final.keys():
#     indicator = ph_final[indicator_id]
#     print(indicator.shape)
# for indicator_id in re_final.keys():
#     indicator = re_final[indicator_id]
#     print(indicator.shape)
# =============================================================================
#%%
#Convert data to numeric dtypes
for indicator_id in ph_final.keys():
    indicator = ph_final[indicator_id]
    years = [column for column in indicator.columns if not isinstance(column, str)]
    indicator[years] = indicator[years].astype(float)
    #print(indicator.dtypes)
    ph_final[indicator_id] = indicator

for indicator_id in re_final.keys():
    indicator = re_final[indicator_id]
    years = [column for column in indicator.columns if not isinstance(column, str)]
    indicator[years] = indicator[years].astype(float)
    #print(indicator.dtypes)
    re_final[indicator_id] = indicator

#%%
#Save data
import pickle
with open('data_v3_final/public_health_data.pkl', 'wb') as f:
    pickle.dump(ph_final, f)

with open('data_v3_final/renewable_energy_data.pkl', 'wb') as f:
    pickle.dump(re_final, f)