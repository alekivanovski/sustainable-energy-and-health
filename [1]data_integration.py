# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 17:54:02 2020

@author: Nikola
"""
import pandas as pd
#%%
# =============================================================================
# #WB data integration
# =============================================================================
#Import WB data as df
esg_df = pd.read_csv(('data_v1_original/WB/')+
                     ('Environment_Social_and_Governance_(ESG)/')+
                     ('Environment_Social_and_Governance_(ESG)_Data.csv'))
sea_df = pd.read_csv(('data_v1_original/WB/')+
                     ('Sustainable_Energy_for_All/')+
                     ('Sustainable_Energy_for_All_Data.csv'))
wdi_df = pd.read_csv(('data_v1_original/WB/')+
                     ('World_Development_Indicators/')+
                     ('World_Development_Indicators_Data.csv'))
#%%
# =============================================================================
# #Clean country values
# =============================================================================
#Drop empty country values
esg_df = esg_df[(esg_df['Country Name'].notnull())]
sea_df = sea_df[(sea_df['Country Name'].notnull())]
wdi_df = wdi_df[(wdi_df['Country Name'].notnull())]
#%%
#Remove comment rows
esg_comments = ['Data from database: Environment Social and Governance (ESG) Data',
       'Last Updated: 07/06/2020']
sea_comments = ['Data from database: Sustainable Energy for All',
       'Last Updated: 06/30/2018']
wdi_comments = ['Data from database: World Development Indicators',
       'Last Updated: 09/16/2020']

esg_df = esg_df[~(esg_df['Country Name'].isin(esg_comments))]
sea_df = sea_df[~(sea_df['Country Name'].isin(sea_comments))]
wdi_df = wdi_df[~(wdi_df['Country Name'].isin(wdi_comments))]
#%%
# =============================================================================
# #Drop redundant columns & rename rest
# =============================================================================
#Drop redundand columns
esg_df = esg_df.drop(columns = ['Country Code','Series Code',
                                '2017 [YR2017]',
                                '2018 [YR2018]', 
                                '2019 [YR2019]'])
sea_df = sea_df.drop(columns = ['Country Code','Series Code'])
wdi_df = wdi_df.drop(columns = ['Country Code','Series Code',
                                '2017 [YR2017]',
                                '2018 [YR2018]', 
                                '2019 [YR2019]',
                                '2020 [YR2020]'])
#%%
#Rename columns
column_format = {'Country Name':'Country',
                         'Series Name':'Indicator',
                         '2000 [YR2000]':2000,
                         '2001 [YR2001]':2001,
                         '2002 [YR2002]':2002,
                         '2003 [YR2003]':2003,
                         '2004 [YR2004]':2004,
                         '2005 [YR2005]':2005,
                         '2006 [YR2012]':2006,
                         '2007 [YR2007]':2007,
                         '2008 [YR2008]':2008,
                         '2009 [YR2009]':2009,
                         '2010 [YR2010]':2010,
                         '2011 [YR2011]':2011,
                         '2012 [YR2012]':2012,
                         '2013 [YR2013]':2013,
                         '2014 [YR2014]':2014,
                         '2015 [YR2015]':2015,
                         '2016 [YR2016]':2016}
esg_df.rename(columns = column_format, inplace = True) 
sea_df.rename(columns = column_format, inplace = True) 
wdi_df.rename(columns = column_format, inplace = True) 
#%%
# =============================================================================
# #Merge wb data into one df
# =============================================================================
#Remove redundant indicators from esg & sea data
#before merging with wdi data
esg_only = esg_df[~(esg_df['Indicator'].isin(wdi_df['Indicator']))]
sea_only = sea_df[~(sea_df['Indicator'].isin(wdi_df['Indicator']))]
#%%
#Merge wb data into one df
wb_df = pd.concat([wdi_df, sea_only,esg_only], ignore_index = True)
wb_df = wb_df.drop_duplicates()
#%%
#Remove unnecessary variables
del column_format, esg_comments, esg_df, esg_only
del sea_comments, sea_df, sea_only, wdi_comments, wdi_df
#%%
# =============================================================================
# #WHO data
# =============================================================================
# =============================================================================
# #Child Mortality Indicators
# =============================================================================
cm_n_alri_df = pd.read_csv(('data_v1_original/WHO/Child mortality/')+
                     ('Child mortality - Number of deaths - Acute lower respiratory infections.csv'),
                     header = [0,1,2,3])
cm_n_ca_df = pd.read_csv(('data_v1_original/WHO/Child mortality/')+
                     ('Child mortality - Number of deaths - Congenital anomalies.csv'),
                     header = [0,1,2,3])
cm_n_p_df = pd.read_csv(('data_v1_original/WHO/Child mortality/')+
                     ('Child mortality - Number of deaths - Prematurity.csv'),
                     header = [0,1,2,3])
cm_rod_alri_df = pd.read_csv(('data_v1_original/WHO/Child mortality/')+
                     ('Child mortality - Rate of deaths - Acute lower respiratory infections.csv'),
                     header = [0,1,2,3])
cm_rod_ca_df = pd.read_csv(('data_v1_original/WHO/Child mortality/')+
                     ('Child mortality - Rate of deaths - Congenital anomalies.csv'),
                     header = [0,1,2,3])
cm_rod_p_df = pd.read_csv(('data_v1_original/WHO/Child mortality/')+
                     ('Child mortality - Rate of deaths - Prematurity.csv'),
                     header = [0,1,2,3])
#%%
def filter_by_child_age_group(column_names, category):
    filtered_columns = []
    filtered_columns.append(column_names[0])
    for column in column_names:
        if column[3] == category:
            filtered_columns.append(column)
    return filtered_columns

def who_data_integration_cm_rename_columns(data,age_group_columns):
    indicator_category = 'Child mortality '
    indicator_name = (indicator_category + '(' + age_group_columns[1][3] + ') '
                  + 'caused by ' + age_group_columns[1][2] + 
                  ' (' + age_group_columns[1][1] + ')')
    #sort by year
    age_group_columns = sorted(age_group_columns)
    # shift last element (country names) to first 
    age_group_columns.insert(0, age_group_columns.pop()) 
    #get only age group columns
    data_age_group = data[age_group_columns]
    #rename columns    
    column_format = []
    for column in age_group_columns:
        if column[3] == 'Country':
            column_format.append('Country')
        else:
            column_format.append(int(column[0]))
    data_age_group.columns = column_format
    #add the indicator column
    data_age_group.insert(1,'Indicator',indicator_name)
    return data_age_group

def who_data_integration_cm(data,age_groups = ['0-27 days',
                                               '1-59 months',
                                               '0-4 years']):
    age_group_1 = filter_by_child_age_group(data.columns, age_groups[0])
    age_group_2 = filter_by_child_age_group(data.columns, age_groups[1])
    age_group_3 = filter_by_child_age_group(data.columns, age_groups[2])
    
    data_1 = who_data_integration_cm_rename_columns(data, age_group_1)
    data_2 = who_data_integration_cm_rename_columns(data, age_group_2)
    data_3 = who_data_integration_cm_rename_columns(data, age_group_3)

    return [data_1,data_2,data_3]

# =============================================================================
# #Test case
# cm_n_alri_df_separated = who_data_integration_cm(cm_n_alri_df)
# =============================================================================
#Integrate all indicators in one list
who_cm = []
who_cm.extend(who_data_integration_cm(cm_n_alri_df))
who_cm.extend(who_data_integration_cm(cm_n_ca_df))
who_cm.extend(who_data_integration_cm(cm_n_p_df))
who_cm.extend(who_data_integration_cm(cm_rod_alri_df))
who_cm.extend(who_data_integration_cm(cm_rod_ca_df))
who_cm.extend(who_data_integration_cm(cm_rod_p_df))

# =============================================================================
# #Print all indicators in category
# for x in who_cm:
#     print(x['Indicator'].unique())
# =============================================================================
#%%
#Remove unnecessary variables
del cm_n_alri_df, cm_n_ca_df, cm_n_p_df, cm_rod_alri_df, cm_rod_ca_df, cm_rod_p_df 
#%%
# =============================================================================
# #Clean Energy Indicators
# =============================================================================
ce_df = pd.read_csv(('data_v1_original/WHO/Clean Energy/')+
                     ('Population with primary reliance on clean fuels and technologies.csv'),
                     header = [0,1])
#Get indicator name
indicator_name = ce_df.columns[1][0]
#Remove indicator name from multiindex column
ce_df.columns = ce_df.columns.droplevel()
#Sort years and separate country column to convert years
ce_df = ce_df.reindex(sorted(ce_df.columns), axis=1)
ce_countries = ce_df['Country']
ce_df = ce_df.drop(columns = ['Country'])
#Convert year columns to int
ce_df.columns = ce_df.columns.map(int)
#Insert Indicator and Reinsert country columns
ce_df.insert(0,'Indicator',indicator_name)
ce_df.insert(0,'Country',ce_countries)
#%%
#Remove unnecessary variables
del ce_countries, indicator_name
#%%
# =============================================================================
# #Mortality and life estimates
# =============================================================================
# =============================================================================
#     #Adult mortality rate
# =============================================================================
mle_amr_df = pd.read_csv(('data_v1_original/WHO/Mortality and life estimates/')+
                     ('Adult mortality rate (probability of dying between 15 and 60 years per 1000 population).csv'),
                     header = [0,1])
#Get indicator name
indicator_name = mle_amr_df.columns[3][0]
#Remove indicator name from multiindex column
mle_amr_df.columns = mle_amr_df.columns.droplevel()
#%%
#Separate indicators by categories and group by year
mle_amr_all_df = mle_amr_df.pivot(index = 'Country', columns = 'Year',values = 'Both sexes')
mle_amr_men_df = mle_amr_df.pivot(index = 'Country', columns = 'Year',values = 'Male')
mle_amr_fem_df = mle_amr_df.pivot(index = 'Country', columns = 'Year',values = 'Female')
#%%
#Return the country column
mle_amr_all_df.reset_index(level=0, inplace=True)
mle_amr_men_df.reset_index(level=0, inplace=True)
mle_amr_fem_df.reset_index(level=0, inplace=True)
#%%
mle_amr_all_df.columns.name = None
mle_amr_men_df.columns.name = None
mle_amr_fem_df.columns.name = None
#%%
#Add indicator name column
mle_amr_all_df.insert(1,'Indicator',indicator_name + ' (Both Sexes)')
mle_amr_men_df.insert(1,'Indicator',indicator_name + ' (Male)')
mle_amr_fem_df.insert(1,'Indicator',indicator_name + ' (Female)')
#%%
#Save to single list
who_mle = [mle_amr_all_df,mle_amr_men_df,mle_amr_fem_df]
del mle_amr_all_df, mle_amr_men_df, mle_amr_fem_df, mle_amr_df
#%%
# =============================================================================
#     #Life expectancy and Healthy life expectancy
# =============================================================================
mle_le_df = pd.read_csv(('data_v1_original/WHO/Mortality and life estimates/')+
                     ('Life expectancy and Healthy life expectancy.csv'),
                     header = [0,1])
def who_data_integration_mle(data):
    who_mle_le = []
    #Get meta-data columns
    meta_data = data.columns[:2]
    #Get indicator name
    indicator_columns = data.columns[2:]
    
    for indicator_column in indicator_columns:
        indicator = indicator_column[0] + ' (' + indicator_column[1] + ')'
        group = indicator_column[1]
        
        subset_columns = list(meta_data)
        subset_columns.append(indicator_column)
        mle_le_1 = data[subset_columns]
        
        #Remove indicator name from multiindex column
        mle_le_1.columns = mle_le_1.columns.droplevel()
        
        #Separate indicators by categories and group by year
        mle_le_pivot = mle_le_1.pivot(index = 'Country', columns = 'Year',values = group)
        #Return the country column
        mle_le_pivot.reset_index(level=0, inplace=True)
        mle_le_pivot.columns.name = None
        #Add indicator name column
        mle_le_pivot.insert(1,'Indicator',indicator)
        who_mle_le.append(mle_le_pivot)
    return who_mle_le

who_mle_le = who_data_integration_mle(mle_le_df)
del mle_le_df
#%%
# =============================================================================
# #Test case
# for i in who_mle_le:
#     print(i['Indicator'].unique())
# =============================================================================
#%%
# =============================================================================
# #Women's health
# =============================================================================
wh_df = pd.read_csv(('data_v1_original/WHO/Women\'s health/')+
                     ('Prevalence of anaemia in women - Estimates by country.csv'))
#%%
def who_data_integration_wh(data):
    who_wh = []
    #Get meta-data columns
    meta_data = data.columns[:2]
    #Get indicator name
    indicator_columns = data.columns[2:]
    
    for indicator in indicator_columns:
        subset_columns = list(meta_data)
        subset_columns.append(indicator)
        wh_temp = data[subset_columns]
        
        #Separate indicators by categories and group by year
        wh_temp = wh_temp.pivot(index = 'Country', columns = 'Year',values = indicator)
        #Return the country column
        wh_temp.reset_index(level=0, inplace=True)
        wh_temp.columns.name = None
        #Add indicator name column
        wh_temp.insert(1,'Indicator',indicator)
        who_wh.append(wh_temp)
    return who_wh

who_wh = who_data_integration_wh(wh_df)
del wh_df
#%%
# =============================================================================
# #Test case
# for i in who_wh:
#     print(i['Indicator'].unique())
# =============================================================================
#%%
#Transform data into numeric values
years = list(range(2000,2017))
for i in who_wh:
    for year in years:
        i[year] = i[year].apply(lambda x: float(x.split()[0]))
del year, years, i
#%%
#Separate indicators by main category (Renewable energy or Public Health)
redundant_indicators = ['Access to Clean Fuels and Technologies for cooking (% of total population)',
       'Access to electricity (% of rural population with access)',
       'Access to electricity (% of total population)',
       'Access to electricity (% of urban population with access)',
       'Energy intensity level of primary energy (MJ/2011 USD PPP)',
       'Renewable electricity share of total electricity output (%)']
wb_df = wb_df[~wb_df['Indicator'].isin(redundant_indicators)]
#%%
wb_health_indicators = ['Mortality rate attributed to household and ambient air pollution, age-standardized (per 100,000 population)',
                        'Mortality rate attributed to unintentional poisoning (per 100,000 population)'
                        ]
wb_ph = wb_df[wb_df['Indicator'].isin(wb_health_indicators)]
wb_re = wb_df[~wb_df['Indicator'].isin(wb_health_indicators)]
del wb_df
#%%
#Separate wb data by indicator
ph_indicators = []
re_indicators = []
for indicator in wb_ph['Indicator'].unique():
    ind_data = wb_ph[wb_ph['Indicator']==indicator]
    ph_indicators.append(ind_data)
for indicator in wb_re['Indicator'].unique():
    ind_data = wb_re[wb_re['Indicator']==indicator]
    re_indicators.append(ind_data)
#%%
wb_ce = wb_re[wb_re['Indicator']=='Access to clean fuels and technologies for cooking (% of population)']
#%%
#Add who data to indicator lists
# =============================================================================
# #WHO indicator 'Proportion of population with primary reliance on clean fuels and technologies (%)'
# #is the same as WB indicator 'Access to clean fuels and technologies for cooking (% of population)'
# #Therefore ce_df will not be added
# =============================================================================

#Public health indicators
ph_indicators.extend(who_cm)
ph_indicators.extend(who_mle)
ph_indicators.extend(who_mle_le)
ph_indicators.extend(who_wh)
#%%
#Save data
import pickle
with open('data_v2_integrated/public_health_data.pkl', 'wb') as f:
    pickle.dump(ph_indicators, f)

with open('data_v2_integrated/renewable_energy_data.pkl', 'wb') as f:
    pickle.dump(re_indicators, f)


