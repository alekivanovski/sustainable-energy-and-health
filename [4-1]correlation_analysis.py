# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 15:27:48 2020

@author: Nikola
"""
import pandas as pd
#%%
#Load data
import pickle
with open('data_v5_corr/correlation_tuples.pkl', 'rb') as f:
    country_corr_tuples = pickle.load(f)

del f
#%%
#Merge all data into one dataframe
all_corr = pd.DataFrame(columns = ['Country','PHI','REI','Corr'])
for country in country_corr_tuples.keys():
    corr_tup = country_corr_tuples[country]
    #corr_tup = corr_tup.fillna(0)
    country_corr_tuples[country] = corr_tup
    all_corr = all_corr.append(corr_tup)
#%%
#Separate by indicator combinations
ind_combo_groups = all_corr.groupby(['PHI','REI'])    
ind_corr_tuples = {x:ind_combo_groups.get_group(x) for x in ind_combo_groups.groups}

del ind_combo_groups
#%%
#Create descriptive stats of combination values
def get_corr_breakdown(corr_data):
    above9 = len(corr_data[abs(corr_data['Corr']) >= 0.9])
    above8 = len(corr_data[abs(corr_data['Corr']) >= 0.8])
    above7 = len(corr_data[abs(corr_data['Corr']) >= 0.7])
    above6 = len(corr_data[abs(corr_data['Corr']) >= 0.6])
    above5 = len(corr_data[abs(corr_data['Corr']) >= 0.5])
    between89 = len(corr_data[(abs(corr_data['Corr']) < 0.9) & (abs(corr_data['Corr']) >= 0.8)])
    between78 = len(corr_data[(abs(corr_data['Corr']) < 0.8) & (abs(corr_data['Corr']) >= 0.7)])
    between67 = len(corr_data[(abs(corr_data['Corr']) < 0.7) & (abs(corr_data['Corr']) >= 0.6)])
    between56 = len(corr_data[(abs(corr_data['Corr']) < 0.6) & (abs(corr_data['Corr']) >= 0.5)])
    below5 = len(corr_data[abs(corr_data['Corr']) < 0.5])
    format_corr = {'>=0.9':above9,'>=0.8':above8,'>=0.7':above7,
                   '>=0.6':above6,'>=0.5':above5,'<0.5':below5,
                   '>=0.8 & <0.9':between89,'>=0.7 & <0.8':between78,
                   '>=0.6 & <0.7':between67,'>=0.5 & <0.6':between56}
    return pd.DataFrame(format_corr, index=['Corr']).transpose()


ind_corr_stats = pd.DataFrame(columns=['PHI', 'REI', 
       'mean', 'std', 'min', '25%', '50%', '75%', 'max', '>=0.9',
       '>=0.8', '>=0.7', '>=0.6', '>=0.5', '<0.5', '>=0.8 & <0.9',
       '>=0.7 & <0.8', '>=0.6 & <0.7', '>=0.5 & <0.6'])
for ind_combo in ind_corr_tuples.keys():
    ind_corr = ind_corr_tuples[ind_combo]
    corr_stats = ind_corr.describe()
    corr_stats = corr_stats.drop(index = ['count'])
    corr_breakdown = get_corr_breakdown(ind_corr)
    corr_all_stats = pd.concat([corr_stats,corr_breakdown]).transpose()
    corr_all_stats.insert(0,'PHI',ind_combo[0])
    corr_all_stats.insert(1,'REI',ind_combo[1])
    ind_corr_stats = ind_corr_stats.append(corr_all_stats)

del corr_stats, corr_breakdown, corr_all_stats, ind_corr, ind_combo
#%%
#Sort by absolute mean value
ind_corr_stats_sorted = ind_corr_stats.iloc[(-ind_corr_stats['mean'].abs()).argsort()]

#Save data in excel table
writer = pd.ExcelWriter('data_v5_corr/indicator_all_combos_corr_stats.xls')
ind_corr_stats_sorted.to_excel(writer)
writer.save()
#%%
#Get direct re indicators
direct_rei = ['Access to clean fuels and technologies for cooking (% of population)',
              'Alternative and nuclear energy (% of total energy use)',
              'Combustible renewables and waste (% of total energy)',
              'Electricity production from coal sources (% of total)',
              'Electricity production from hydroelectric sources (% of total)',
              'Electricity production from natural gas sources (% of total)',
              'Electricity production from nuclear sources (% of total)',
              'Electricity production from oil sources (% of total)',
              'Electricity production from oil, gas and coal sources (% of total)',
              'Electricity production from renewable sources, excluding hydroelectric (% of total)',
              'Electricity production from renewable sources, excluding hydroelectric (kWh)',
              'Fossil fuel energy consumption (% of total)',
              'Renewable electricity output (% of total electricity output)',
              'Renewable electricity output (GWh)',
              'Renewable energy consumption (% of total final energy consumption)',
              'Renewable energy consumption (TJ)',
              'Renewable energy share of TFEC (%)']
#%%
#Get all indicator combos with only direct re indicators
ind_corr_stats_sorted_direct = ind_corr_stats_sorted[ind_corr_stats_sorted['REI'].isin(direct_rei)]

#Save data in excel table
writer = pd.ExcelWriter('data_v5_corr/indicator_all_direct_combos_corr_stats.xls')
ind_corr_stats_sorted_direct.to_excel(writer)
writer.save()
#%%
import matplotlib.pyplot as plt
import seaborn as sns
#Get histogram for mean correlation for all indicator combos
def plot_histogram_of_mean_corr(data,
                                combo_types = 'all',
                                folder = 'data_v5_corr/',
                                show=False):
    
    sns.set(style="darkgrid")
    #Plot Histogram with kernel density estimate
    sns.distplot(data['mean'], bins = 40)
    plt.title(('Histogram with kernel density estimate for \n'+
               combo_types+' indicator combinations'))
    
    name = ('Histogram for '+combo_types+' indicator combinations')
    plt.savefig(folder+name+'.png',dpi=200,bbox_inches='tight')
    if show:
        plt.show()
    plt.close()

plot_histogram_of_mean_corr(ind_corr_stats_sorted)
#Do the same for indicator combos with direct re indicators
plot_histogram_of_mean_corr(ind_corr_stats_sorted_direct,'all direct RE')
#%%
import numpy as np
#Get histogram for correlation values per indicator combo
def plot_histogram_of_corr_vals(data,number = 0,
                                folder = 'data_v5_corr/top_25_histograms/',
                                show=False):
    
    sns.set(style="darkgrid")
    #Plot Histogram with kernel density estimate
    ax = sns.distplot(data['Corr'], bins=60,kde=False)
    ax.set(xlabel='Correlation')
    plt.xticks(np.arange(-1.0, 1.0001, 0.1),rotation=45,size = 10)
    plt.title(('Histogram for correlation values between\n'+
               data['PHI'].iloc[0]+' and\n'+
               data['REI'].iloc[0]))
    
    name = ('Histogram ['+str(number)+']')
    plt.savefig(folder+name+'.png',dpi=200,bbox_inches='tight')
    if show:
        plt.show()
    plt.close()

#Get top 25 direct re indicator combos
top_25_direct = ind_corr_stats_sorted_direct.iloc[0:25]

#Plot their correlation values
for i in range(len(top_25_direct)):
    corr_combo = all_corr[(all_corr['PHI'] == top_25_direct['PHI'].iloc[i]) &
                      (all_corr['REI'] == top_25_direct['REI'].iloc[i])]
    plot_histogram_of_corr_vals(corr_combo,i+1)
#%%
writer = pd.ExcelWriter('data_v5_corr/indicator_all_correlations_by_country.xls')
#Sort values in by country tuples (by abs value)
for country in country_corr_tuples.keys():
    corr_tup = country_corr_tuples[country].copy()
    corr_tup = corr_tup.drop(columns = ['Country'])
    corr_tup = corr_tup.rename(columns={'Corr': 'Correlation'})
    #corr_tup = corr_tup.fillna(0)
    #Sort by absolute mean value
    corr_tup = corr_tup.iloc[(-corr_tup['Correlation'].abs()).argsort()]
    #Save to excel sheet
    corr_tup.to_excel(writer,sheet_name = country)

#Save all sheets
writer.save()
#%%
def get_only_combos_in_subset(data,subset):
    data_new = pd.DataFrame(columns=data.columns)
    for i in range(len(subset)):
        corr_combo = data[(data['PHI'] == subset['PHI'].iloc[i]) &
                          (data['REI'] == subset['REI'].iloc[i])]
        data_new = data_new.append(corr_combo)
    return data_new

writer = pd.ExcelWriter('data_v5_corr/indicator_top_25_correlations_by_country.xls')
#Sort values in by country tuples (by abs value)
for country in country_corr_tuples.keys():
    corr_tup = country_corr_tuples[country].copy()
    corr_tup = corr_tup.drop(columns = ['Country'])
    corr_tup = corr_tup.rename(columns={'Corr': 'Correlation'})
    corr_tup_subset = get_only_combos_in_subset(corr_tup,top_25_direct)
    #corr_tup = corr_tup.fillna(0)
    #Sort by absolute mean value
    corr_tup_subset = corr_tup_subset.iloc[(-corr_tup_subset['Correlation'].abs()).argsort()]
    #Save to excel sheet
    corr_tup_subset.to_excel(writer,sheet_name = country)

#Save all sheets
writer.save()
#%%