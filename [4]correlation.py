# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 17:54:02 2020

@author: Nikola
"""
import pandas as pd
#%%
#Load data
import pickle
with open('data_v3_final/public_health_data.pkl', 'rb') as f:
    ph_indicators = pickle.load(f)

with open('data_v3_final/renewable_energy_data.pkl', 'rb') as f:
    re_indicators = pickle.load(f)
    
del f
#%%
#Add category column
for indicator_id in ph_indicators.keys():
    indicator = ph_indicators[indicator_id]
    indicator.insert(2,'Category','Public Health')
    ph_indicators[indicator_id] = indicator

for indicator_id in re_indicators.keys():
    indicator = re_indicators[indicator_id]
    indicator.insert(2,'Category','Renewable Energy')
    re_indicators[indicator_id] = indicator
#%%
#Separate indicators by country
indicators_by_country = {}
all_indicators = pd.DataFrame(columns = indicator.columns)
for indicator_id in ph_indicators.keys():
    indicator = ph_indicators[indicator_id]
    all_indicators = all_indicators.append(indicator)
    
for indicator_id in re_indicators.keys():
    indicator = re_indicators[indicator_id]
    all_indicators = all_indicators.append(indicator)
    
for country in all_indicators['Country'].unique():
    indicators_by_country[country] = all_indicators[all_indicators['Country'] == country]
#%%
def create_pearson_correlation_matrix(data):
    country_data = data
    years = [column for column in country_data.columns if not isinstance(column, str)]
    years.insert(0,'Indicator')
    corr_matrix = country_data[years]
    corr_matrix = corr_matrix.set_index('Indicator')
    corr_matrix = corr_matrix.transpose()
    corr_matrix = corr_matrix.corr(method='pearson')
    return corr_matrix

country_pear_corr = {}
for country in indicators_by_country.keys():
    country_data = indicators_by_country[country]
    country_corr = create_pearson_correlation_matrix(country_data)
    country_pear_corr[country] = country_corr
#%%
def convert_corr_matrix_to_tuples(corr_mats,ph_ind_keys,re_ind_keys,show = False):
    corr_tuples = {}
    for country in corr_mats.keys():
        if show:
            print(country)
        country_corr = corr_mats[country]
        corr_values = pd.DataFrame(columns = ['Country','PHI','REI','Corr'])
        for row_index, row in country_corr.iterrows():
            for column_index in list(row.index):
                corr_values = corr_values.append({'Country':country,
                                    'PHI':row_index,
                                    'REI':column_index,
                                    'Corr':row[column_index]},
                                    ignore_index=True)
        
        corr_values = corr_values[corr_values['PHI'].isin(ph_ind_keys)]
        corr_values = corr_values[corr_values['REI'].isin(re_ind_keys)]
        corr_tuples[country] = corr_values
    return corr_tuples

country_corr_tuples = convert_corr_matrix_to_tuples(country_pear_corr,
                                                    list(ph_indicators.keys()),
                                                    list(re_indicators.keys()),
                                                    True)
#%%
# =============================================================================
# #Test shapes
# for country in country_corr_tuples.keys():
#     corr_tuples = country_corr_tuples[country]
#     print(corr_tuples.shape)
# =============================================================================
#%%
#Save data
import pickle
with open('data_v5_corr/correlation_tuples.pkl', 'wb') as f:
    pickle.dump(country_corr_tuples, f)
    


'''
#%%
def extract_high_corr(country_corr_data, ph_ind, re_ind, threshold = 0.8):
    country_high_corr = {}
    for country in country_corr_data.keys():
        country_corr = country_corr_data[country]
        print(country)
        corr_values = pd.DataFrame(columns = ['PHI','REI','Corr'])
        for row_index, row in country_corr.iterrows():
            for column_index in list(row.index):
                if row_index in ph_ind and column_index in re_ind:
                    corr_values = corr_values.append({'PHI':row_index,
                                         'REI':column_index,
                                         'Corr':row[column_index]},
                                       ignore_index=True)
                    
        corr_high = corr_values[abs(corr_values['Corr']) > threshold]
        country_high_corr[country] = corr_high
    return country_high_corr

country_high_corr = extract_high_corr(country_pear_corr,
                                      list(ph_indicators.keys()),
                                      list(re_indicators.keys()))
#%%
high_corr_all = pd.DataFrame(columns = ['PHI','REI','Corr'])
for country in country_high_corr.keys():
        country_df = country_high_corr[country]
        high_corr_all = high_corr_all.append(country_df)

#%%
writer = pd.ExcelWriter('data_v5_corr/high_corr_all.xls') # Init Pandas excel writer, using the file name 'baseball.xlsx'
high_corr_all.to_excel(writer) # Writes to a sheet called 'baseball_sheet'. Format follows the Dataframe format.
writer.save()
#%%
high_corr_count = high_corr_all.groupby(['PHI','REI']).size().reset_index().rename(columns={0:'count'}).sort_values('count',ascending=False)
#%%
high_corr_count_keep = high_corr_count[high_corr_count['count'] > 55]

direct_renewable_indicators = ['Access to clean fuels and technologies for cooking (% of population)',
 'CO2 emissions (kg per PPP $ of GDP)',
 'Renewable energy consumption (% of total final energy consumption)',
 'Renewable energy share of TFEC (%)',
 'Renewable energy consumption (TJ)',
 'Energy intensity level of primary energy (MJ/$2011 PPP GDP)',
 'Combustible renewables and waste (% of total energy)',
 'Electricity production from renewable sources, excluding hydroelectric (kWh)']

high_corr_count_keep = high_corr_count_keep[high_corr_count_keep['REI'].isin(direct_renewable_indicators)]
#%%
writer = pd.ExcelWriter('data_v5_corr/high_corr_best.xls') # Init Pandas excel writer, using the file name 'baseball.xlsx'
high_corr_count_keep.to_excel(writer) # Writes to a sheet called 'baseball_sheet'. Format follows the Dataframe format.
writer.save()
#%%
high_corr_final_separate = high_corr_count_keep.groupby(['PHI','REI'])
high_corr_final_groups = [group for group in high_corr_final_separate.groups]
#%%
def extract_all_corr(country_corr_data, ph_ind, re_ind):
    country_high_corr = {}
    for country in country_corr_data.keys():
        country_corr = country_corr_data[country]
        print(country)
        corr_values = pd.DataFrame(columns = ['PHI','REI','Corr'])
        for row_index, row in country_corr.iterrows():
            for column_index in list(row.index):
                if row_index in ph_ind and column_index in re_ind:
                    corr_values = corr_values.append({'PHI':row_index,
                                         'REI':column_index,
                                         'Corr':row[column_index]},
                                       ignore_index=True)
                    
        country_high_corr[country] = corr_values
    return country_high_corr

country_all_corr = extract_all_corr(country_pear_corr,
                                      list(ph_indicators.keys()),
                                      list(re_indicators.keys()))
#%%
country_corr_all_merge = pd.DataFrame(columns = ['PHI','REI','Corr'])
for country in country_all_corr.keys():
        country_df = country_all_corr[country]
        country_corr_all_merge = country_corr_all_merge.append(country_df)

country_corr_all_merge_groupby = country_corr_all_merge.groupby(['PHI','REI'])
country_corr_all_corr_final_groups = [country_corr_all_merge_groupby.get_group(group) for group in high_corr_final_groups]
#%%
for country in country_high_corr.keys():
    country_df = country_high_corr[country]
    print(country_df.shape)

#%%
'''