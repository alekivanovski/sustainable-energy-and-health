**Analysis of the correlation between sustainable energy and public health**
# 1. Download data
For the purposes of this analysis, we used indicators from 2 major sources: [World Bank](https://databank.worldbank.org/home.aspx) & World Health Organization([WHO](https://apps.who.int/gho/data/node.main)).
In our initial investigation, these were the indicators selected as being applicable to our problem.
As this is a time series analysis, we have to consider a time range that is both relevant and not unnecessarily large. Therefore the initial time range is between the years 2000-2020.
## WHO data
The vast majority of the WHO data were in relation to public health. Unfortunately, most of the data collection was not done at an annual basis, therefore we concluded that we would consider that data as incomplete and therefore discarded from the start, except in rare cases, as any form of analysis done on that type of data would not be sufficient for any valid conclusions.
The few datasets that were found of use were in the following categories:
- [Child mortality](https://apps.who.int/gho/data/node.main.COCD?lang=en)
- [Clean Energy](https://apps.who.int/gho/data/node.main.GSWCAH38?lang=en)
- [Mortality](https://apps.who.int/gho/data/node.main.11?lang=en) and [life estimates](https://apps.who.int/gho/data/node.main.688?lang=en)
- [Women's health](https://apps.who.int/gho/data/node.main.GSWCAH28?lang=en)
## World Bank data
World Bank has a much larger variety of areas of interest and more data in each area. This results in a much larger database, however on further inspection, there was significant repetition of indicators in the different datasets. So, in the preprocessing step this will have to be adressed. Also, the integrity of the data was decided to be analysed in preprocessing as the indicators were not separate as in the case of WHO, so it allowed for a single query for each dataset.
The following datasets were used, having both indicators connected to sustainable energy and public health:
- [World Development Indicators](https://databank.worldbank.org/source/world-development-indicators)
- [Environment Social and Governance (ESG) Data](https://databank.worldbank.org/source/environment-social-and-governance-(esg)-data)
- [Sustainable Energy for All](https://databank.worldbank.org/source/sustainable-energy-for-all)
# 2. Data preprocessing
## Data Integration
Before doing any sort of transformation on the data, we had to prepare it to be uniform. 
The step taken were:
- Removing the redundant indicators
- Removing the redundant columns (The necessary columns were the country name, indicator name and all of the years within the time range)
- Separating the indicators into dataframes
- Transforming those dataframes to the same form
    - Renaming the columns
    - Sorting the years

The resulting data was separated into two lists, one for Sustainable Energy Indicators and one for Public Health Indicators.
A backup of this integrated data is saved [here](https://gitlab.com/alekivanovski/sustainable-energy-and-health/-/tree/master/data_v2_integrated) in pickled form.

## Missing Values
While the data is of high quality, as all the non-missing values were numeric and could immediately be converted from string format, there were several indicators which had large numbers of missing values.
The first serious issue was the lack of data past 2015. While some indicators had some data from 2016 and 2017, it was not enough to be considered useful. This is why in this step we decided to shorten the time range to [2005-2015], as anything before or after this time range was very inconsistent.
There were still some indicators, even in this time range which had many missing values.
As transforming them by hand would be tedious, an automated approach was taken.
The algorithm for dealing with indicators with a lot of missing values was as follows:
- For each indicator, generate a missing values matrix, where True is assigned if there is a value in that data point, and False if there is a missing value.
- From each missing values matrix, generate 2 statistics, one counting the number of occurences of a combination of True/False values for all of the years (ex. If only one country has True values for all years except the first year, that combination [False, True, ..., True] will have a count of 1) and one counting the number of non-missing values per country.
- From the combinations statistic, generate a summary for each combination of how many True values does it contain, how many countries in the indicator have that combination, and what percentage do the True values take.
- From this summary, remove the indicators for which the largest number of countries have a combination which has more than 1 missing value.

After this step the number of indicators went down from 38 to 30 for Public Health Indicators, and from 62 to 47 for Sustainable Energy Indicators.

For the remaining data, there were still indicators which had a single missing value for several countries.
Simply filling the missing value with the previous or next value in the time series for example could lead to problems for the analysis down the line, so we opted for spline interpolation to fill in the missing values.

## Country Name Integration
As we can't generate the missing data for countries for an entire time series in an indicator, we considered the intersection of countries available in all indicators to be a adequate subset of the data.
However much like the format of the data, the names of the countries in different indicators were inconsistent.
To solve this issue, we took both the intersection and the symmetric difference of the sets of countries for each indicator.

Within the symmetric difference we found several instances of different names for the same country. After mending these cases, the original intersection of 94 countries increased to 103 countries.


So, the final version of the data consists of 77 indicators, 30 for Public Health and 47 for Sustainable Energy, for 103 countries for the time range of [2005-2015], a backup of which is saved [here](https://gitlab.com/alekivanovski/sustainable-energy-and-health/-/tree/master/data_v3_final).

# 3. Outlier analysis
## Rates of change
Before we can do any form of technical analysis, we first need to analyse the data for anomalies like outliers, and for that reason, we have to visualize the data values and the rates of changes of those values.

For each indicator, we generated 2 matrices containing the rates of change between every two consecutive years for each country, one rate of change being the difference between the values of the two years, and the other being the [percentage change](https://en.wikipedia.org/wiki/Relative_change_and_difference#Percentage_change) between the two years.

## Descriptive statistics
For these 2 matrices of rates of change and the data values matrix, we calculated the descriptive statistics (mean, standard deviation, 25%, 50%, 75%, minimum and maximum). These statistics were then ploted as continuous lines over the time range. A backup is saved for all of these [plots](https://gitlab.com/alekivanovski/sustainable-energy-and-health/-/tree/master/data_v4_plots/Line%20plots).

## Histograms and boxplots
For the 2 matrices of rates of change and the data values matrix, we also ploted [histograms](https://gitlab.com/alekivanovski/sustainable-energy-and-health/-/tree/master/data_v4_plots/Histograms) with a kernel density estimate and a [boxplot](https://gitlab.com/alekivanovski/sustainable-energy-and-health/-/tree/master/data_v4_plots/Boxplots) with separate boxplots for each year.

## Analysis
From the initial analysis of these plots, and subsequently the data, there don't seem to be any anomalies. While there are very obvious outliers in the case of some indicators, there are perfectly valid explanations for these cases. 
For example, in all of the Life Expectancy indicators there was a sudden change for the minimum in the year 2010 which returned to normal the next year. As this was for the case of only a single country it is very abnormal. However, upon further inspection, that country was Haiti and the sudden shift in Life Expectancy was due to the [2010 Haiti earthuake](https://en.wikipedia.org/wiki/Haiti_2010_earthquake).

In the same manner a lot of the rates of change that have large spikes are primarily due to percentage-based indicators, and developing countries can have changes of several percent a year, which as a rate of change is significant if the starting value is very low (ex. 1% to 5% is a 400% increase, while 51% to 55% is a 7.84% increase).

# 4. Correlation
## Pearson Correlation
In the final step of the analysis, we calculated the correlation between each Public Health indicator and each Sustainable Energy indicator.
This was created for each country separately, initally into a 77x77 Pearson Correlation Matrix, and then exctracting only the combinations that we required. These combinations were saved in a backup file for convenience.

Then, all of this data was integrated into a single dataframe from which we conducted our analysis.

## Statistics and Analysis
Firstly, all of the correlation values were separated by indicator combinations, rather than by country. This way we were able to extract statistics for each indicator combination, and have an overview for how correlated the indicators are for all countries. Also, for each further step we also did the same for a subset of combinations with Sustainable Energy indicators which we believe to be of direct interest.
The usual descriptive statistics were used (mean, standard deviation, minimum, maximum, 25%, 50%, 75%), as well as counting the number of countries for which the correlation value was:
- \>=|0.9|
- \>=|0.8|
- \>=|0.7|
- \>=|0.6|
- \>=|0.5|
- \<|0.5|
- \>=|0.8| & \<|0.9|
- \>=|0.7| & \<|0.8|
- \>=|0.6| & \<|0.7|
- \>=|0.5| & \<|0.6|

This sort of separation of values by counting allowed for a more clear view of how these values were distributed.
To gain further insight we made histograms of the mean correlation value for all indicator combinations.

For convenience, we also saved all of these statistics in an excel spreadsheet, sorted by highest absolute mean value.

In order to analyse the most interesting indicator combinations, we selected the 25 indicator combinations with direct interest with the highest absolute mean value.
For all of these indicator combinations we made [histograms](https://gitlab.com/alekivanovski/sustainable-energy-and-health/-/tree/master/data_v5_corr/top_25_histograms) plotting the correlation values for each country.

Finally we saved all of the correlation values per country in an excel spreadsheet, with one sheet per country, sorted by highest correlation value, for all indicator combinations and for the top 25 indicator combinations with direct interest with the highest absolute mean value.