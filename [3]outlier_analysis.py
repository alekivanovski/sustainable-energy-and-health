# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 17:54:02 2020

@author: Nikola
"""
import pandas as pd
#%%
#Load data
import pickle
with open('data_v3_final/public_health_data.pkl', 'rb') as f:
    ph_indicators = pickle.load(f)

with open('data_v3_final/renewable_energy_data.pkl', 'rb') as f:
    re_indicators = pickle.load(f)
    
del f
#%%
def create_matrix_rate_of_change_difference(indicator_group):
    roc_diff = {}
    for indicator_id in indicator_group.keys():
        indicator = indicator_group[indicator_id]
        #Get only the years from the columns
        years = [column for column in indicator.columns if not isinstance(column, str)]
        #Create rate of change matrix (Difference)
        roc_mat_diff = indicator[years].diff(axis=1)
        roc_mat_diff = roc_mat_diff.drop(columns = [2005])
        roc_mat_diff.insert(0,'Indicator',indicator['Indicator'])
        roc_mat_diff.insert(0,'Country',indicator['Country'])
        roc_diff[indicator_id] = roc_mat_diff
    return roc_diff

ph_roc_diff = create_matrix_rate_of_change_difference(ph_indicators)
re_roc_diff = create_matrix_rate_of_change_difference(re_indicators)
#%%
import numpy as np
def create_matrix_rate_of_change_percentage_change(indicator_group):
    roc_pct_change = {}
    for indicator_id in indicator_group.keys():
        indicator = indicator_group[indicator_id]
        #Get only the years from the columns
        years = [column for column in indicator.columns if not isinstance(column, str)]
        #Create rate of change matrix (Percentage change)
        roc_mat_pct_change = indicator[years].pct_change(axis=1)
        #Change all values of np.inf to +1.0 (100% increase)
        
        roc_mat_pct_change = roc_mat_pct_change.replace({np.inf:1.0})
        roc_mat_pct_change = roc_mat_pct_change.drop(columns = [2005])
        roc_mat_pct_change = roc_mat_pct_change.fillna(0)
        roc_mat_pct_change.insert(0,'Indicator',indicator['Indicator'])
        roc_mat_pct_change.insert(0,'Country',indicator['Country'])
        roc_pct_change[indicator_id] = roc_mat_pct_change
    return roc_pct_change

ph_roc_pct_change = create_matrix_rate_of_change_percentage_change(ph_indicators)
re_roc_pct_change = create_matrix_rate_of_change_percentage_change(re_indicators)
#%%
#Generate Descriptive Statistics
def get_descriptive_stats(ind_vals, ind_roc_diffs, ind_roc_pcts):
    val_desc = {}
    roc_diff_desc = {}
    roc_pct_desc = {}
    for indicator_id in ind_vals.keys():
        ind_val = ind_vals[indicator_id]
        ind_roc_diff = ind_roc_diffs[indicator_id]
        ind_roc_pct = ind_roc_pcts[indicator_id]
        
        ind_val_desc = ind_val.describe().transpose()
        ind_val_desc = ind_val_desc.drop('count', 1)
        val_desc[indicator_id] = ind_val_desc
        
        ind_roc_diff_desc = ind_roc_diff.describe().transpose()
        ind_roc_diff_desc = ind_roc_diff_desc.drop('count', 1)
        roc_diff_desc[indicator_id] = ind_roc_diff_desc
        
        ind_roc_pct_desc = ind_roc_pct.describe().transpose()
        ind_roc_pct_desc = ind_roc_pct_desc.drop('count', 1)
        roc_pct_desc[indicator_id] = ind_roc_pct_desc
    return val_desc, roc_diff_desc, roc_pct_desc
        
ph_ind_desc, ph_roc_diff_desc, ph_roc_pct_desc = get_descriptive_stats(ph_indicators,ph_roc_diff,ph_roc_pct_change)
re_ind_desc, re_roc_diff_desc, re_roc_pct_desc = get_descriptive_stats(re_indicators,re_roc_diff,re_roc_pct_change)
#%%
#Show Descriptive Statistics
def show_descriptive_stats(desc_group,data_type):
    for indicator_id in desc_group.keys():
        desc_df = desc_group[indicator_id]
        print(data_type + ' describe statistics for')
        print(indicator_id)
        print(desc_df)
    return

# =============================================================================
# show_descriptive_stats(ph_ind_desc,'Data values')
# show_descriptive_stats(re_ind_desc,'Data values')
# show_descriptive_stats(ph_roc_diff_desc,'Rate of Change - Difference')
# show_descriptive_stats(re_roc_diff_desc,'Rate of Change - Difference')
# show_descriptive_stats(ph_roc_pct_desc,'Rate of Change - Percentage Change')
# show_descriptive_stats(re_roc_pct_desc,'Rate of Change - Percentage Change')
# =============================================================================
#%%
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.legend import Legend

#Plot Descriptive Statistics
def plot_descriptive_stats(indicator,
                           indicator_id,
                           data_type = 'values',
                           folder = 'data_v4_plots/',
                           show=False):
    sns.set(style="darkgrid")
    #Plot all Descriptive Statistics except std
    ax = sns.lineplot(data=indicator.drop('std',axis=1), 
                      marker = 'o',
                      dashes=False)
    #Add label to x-axis
    ax.set(xlabel='Year')
    #Set ticks for x-axis to all years
    ax.set(xticks=list(indicator.index))
    #Plot std as shaded region around mean
    shaded_region = plt.fill_between(list(indicator.index),
                     indicator['mean']-indicator['std'],
                     indicator['mean']+indicator['std'], alpha=.3)
    plt.title(('Descriptive Statistics for '+data_type+' of\n'+
               indicator_id))
    #Add legend outside of plot
    plt.legend(loc='center right',bbox_to_anchor=(1.25, 0.5))
    #Add secondary legend for shaded region
    leg = Legend(ax, [shaded_region], ['std'],
                 loc='lower right', bbox_to_anchor=(1.212, 0.1))
    ax.add_artist(leg)
    
    #Naming convention
    if indicator_id == 'CO2 emissions from other sectors, excluding residential buildings and commercial and public services (% of total fuel combustion)':
        indicator_id = 'CO2 emissions from other sectors (% of total fuel combustion)'
    indicator_id = indicator_id.replace('/', '-')
    name = ('Descriptive Statistics for '+data_type+' of '+
               indicator_id)
    plt.savefig(folder+name+'.png',dpi=200,bbox_inches='tight')
    if show:
        plt.show()
    plt.close()

def plot_all_desc_stats_in_group(indicator_group,data_type = 'values',
                           folder = 'data_v4_plots/'):
    for indicator_id in indicator_group.keys():
        indicator = indicator_group[indicator_id]
        plot_descriptive_stats(indicator,indicator_id,
                               data_type,folder,show=False)

plot_all_desc_stats_in_group(ph_ind_desc,'values','data_v4_plots/Line plots/values/')
plot_all_desc_stats_in_group(re_ind_desc,'values','data_v4_plots/Line plots/values/')
plot_all_desc_stats_in_group(ph_roc_diff_desc,'RoC-Difference','data_v4_plots/Line plots/roc_diff/')
plot_all_desc_stats_in_group(re_roc_diff_desc,'RoC-Difference','data_v4_plots/Line plots/roc_diff/')
plot_all_desc_stats_in_group(ph_roc_pct_desc,'RoC-Percentage','data_v4_plots/Line plots/roc_prc/')
plot_all_desc_stats_in_group(re_roc_pct_desc,'RoC-Percentage','data_v4_plots/Line plots/roc_prc/')

#%%
#Plot Histogram with kernel density estimate
def plot_histogram_with_kde(indicator,
                           indicator_id,
                           data_type = 'values',
                           folder = 'data_v4_plots/',
                           show=False):
    
    sns.set(style="darkgrid")
    #Get only the years from the columns
    years = [column for column in indicator.columns if not isinstance(column, str)]
    #Plot Histogram with kernel density estimate
    sns.distplot(indicator[years], bins = 30)
    plt.title(('Histogram with kernel density estimate for '+data_type+' of\n'+
               indicator_id))
    
    #Naming convention
    if indicator_id == 'CO2 emissions from other sectors, excluding residential buildings and commercial and public services (% of total fuel combustion)':
        indicator_id = 'CO2 emissions from other sectors (% of total fuel combustion)'
    indicator_id = indicator_id.replace('/', '-')
    name = ('Histogram for '+data_type+' of '+
               indicator_id)
    plt.savefig(folder+name+'.png',dpi=200,bbox_inches='tight')
    if show:
        plt.show()
    plt.close()
    
def plot_all_histograms_in_group(indicator_group,data_type = 'values',
                           folder = 'data_v4_plots/'):
    for indicator_id in indicator_group.keys():
        indicator = indicator_group[indicator_id]
        plot_histogram_with_kde(indicator,indicator_id,
                               data_type,folder,show=False)

plot_all_histograms_in_group(ph_indicators,'values','data_v4_plots/Histograms/values/')
plot_all_histograms_in_group(re_indicators,'values','data_v4_plots/Histograms/values/')
plot_all_histograms_in_group(ph_roc_diff,'RoC-Difference','data_v4_plots/Histograms/roc_diff/')
plot_all_histograms_in_group(re_roc_diff,'RoC-Difference','data_v4_plots/Histograms/roc_diff/')
plot_all_histograms_in_group(ph_roc_pct_change,'RoC-Percentage','data_v4_plots/Histograms/roc_prc/')
plot_all_histograms_in_group(re_roc_pct_change,'RoC-Percentage','data_v4_plots/Histograms/roc_prc/')
#%%
def plot_boxplot(indicator,
                           indicator_id,
                           data_type = 'values',
                           folder = 'data_v4_plots/',
                           show=False):
    sns.set(style="darkgrid")
    #Get only the years from the columns
    years = [column for column in indicator.columns if not isinstance(column, str)]
    #Plot Histogram with kernel density estimate
    ax = sns.boxplot(x='variable',y='value',data = pd.melt(indicator[years]))
    #Add label to x-axis
    ax.set(xlabel='Year')
    #Remove label from y-axis
    ax.set(ylabel='')
    plt.title(('Boxplot for '+data_type+' of\n'+
               indicator_id))
    
    #Naming convention
    if indicator_id == 'CO2 emissions from other sectors, excluding residential buildings and commercial and public services (% of total fuel combustion)':
        indicator_id = 'CO2 emissions from other sectors (% of total fuel combustion)'
    indicator_id = indicator_id.replace('/', '-')
    name = ('Boxplot for '+data_type+' of '+
               indicator_id)
    plt.savefig(folder+name+'.png',dpi=200,bbox_inches='tight')
    if show:
        plt.show()
    plt.close()
    
def plot_all_boxplots_in_group(indicator_group,data_type = 'values',
                           folder = 'data_v4_plots/'):
    for indicator_id in indicator_group.keys():
        indicator = indicator_group[indicator_id]
        plot_boxplot(indicator,indicator_id,
                               data_type,folder,show=False)

plot_all_boxplots_in_group(ph_indicators,'values','data_v4_plots/Boxplots/values/')
plot_all_boxplots_in_group(re_indicators,'values','data_v4_plots/Boxplots/values/')
plot_all_boxplots_in_group(ph_roc_diff,'RoC-Difference','data_v4_plots/Boxplots/roc_diff/')
plot_all_boxplots_in_group(re_roc_diff,'RoC-Difference','data_v4_plots/Boxplots/roc_diff/')
plot_all_boxplots_in_group(ph_roc_pct_change,'RoC-Percentage','data_v4_plots/Boxplots/roc_prc/')
plot_all_boxplots_in_group(re_roc_pct_change,'RoC-Percentage','data_v4_plots/Boxplots/roc_prc/')